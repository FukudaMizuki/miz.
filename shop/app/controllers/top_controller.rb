class TopController < ApplicationController
  def main
    unless session[:cart_id]
      cart = Cart.create
      session[:cart_id] = cart.id
    end
    
    @products = Product.all
  end
end
