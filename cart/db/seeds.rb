Product.delete_all
Product.create! id: 1, name: "冷蔵庫", price: 5000, active: true
Product.create! id: 2, name: "洗濯機", price: 10000, active: true
Product.create! id: 3, name: "電子レンジ", price: 3000, active: true
Product.create! id: 4, name: "掃除機", price: 8000, active: true

OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"