class TweetsController < ApplicationController
  
  def main
    if session[:login_uid] then
        redirect_to "/tweets/index"
    end
  end
  def login
    user = User.find_by(uid: params[:uid],pass: params[:pass])
    if user then 
        session[:login_uid] = params[:uid]
        redirect_to "/"
    else 
        render "error"
    end
  end
  def logout
    session.delete(:login_uid)
    redirect_to "/"
  end
  
  def newuser
  end
  
  def usercreate
    if params[:pass1] == params[:pass2] then
      user=User.new(uid: params[:uid],pass: paramas[:pass1])
      user.save
      redirect_to "/"
    else
      @error = "パスワードが違います"
      render "newuser"
    end
  end
  
  def index
    @error=nil
    @tweets = Tweet.all.reverse_order
    logger.debug "川邉"
  end
  
  def new
    @tweet=Tweet.new
  end
  
  def create
   tweet=Tweet.new(message: params[:tweet][:message],tdate: Time.current)
   tweet.save
   redirect_to root_path
  end

  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to root_path
  end

  def show
    @tweet = Tweet.find(params[:id])
  end

  def edit
    @tweet = Tweet.find(params[:id])
  end

  def update
    tweet = Tweet.find(params[:id])
    tweet.update(message: params[:message],tdate: Time.current)
    redirect_to root_path
  end
end